## Visualizing Confusion Matrices

### Directory Structure

Directory contains data for 7 models named: model1, model2, model5, model10, model30, model60 and modelclean (with increasing top-k accuracy from left to right). Every model directory has these files:

* cm\_model1\_nodiag.pdf: PDF visualization of confusion matrices
* confidencescore\_model1.json: JSON file with confidence score of all the test files. JSON contains 2d array of size 5794x200 (5794 test images and 200 classes).
* confusion\_matrix\_model1.json: JSON file with confusion matrix of the model. JSON contains 200x200 matrix.
* probs\_a1\_5frame.npy: raw file with confidence scores can be ignored.

### Raw Images
Dataset can be downloaded from [here](http://www.vision.caltech.edu/visipedia/CUB-200-2011.html) which contains raw images and ground truth values of all the test images.

#### Usage
Simply use the plot\_confmat.py script to generate these files. Might have to edit the filename inside the script.

```
$ python plot_confmat.py
```
