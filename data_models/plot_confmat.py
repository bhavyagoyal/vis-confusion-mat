import matplotlib 
import numpy as np
import json 
import matplotlib.pyplot as plt
from scipy.special import softmax 
from sklearn.metrics import confusion_matrix
from scipy.sparse import csr_matrix
from scipy.sparse.csgraph import reverse_cuthill_mckee
matplotlib.use('Agg')
plt.style.use('ggplot')
matplotlib.rcParams['text.color'] = 'black'
matplotlib.rcParams['axes.labelcolor'] = 'black'
matplotlib.rcParams['xtick.color'] = 'black'
matplotlib.rcParams['ytick.color'] = 'black'
#matplotlib.rcParams.update({'font.size': 16})

foldername = 'modelclean'

probs = np.load(foldername + '/probs_clean.npy')
lines = open('val.txt').readlines()
classes = [int(x.strip().split()[1]) for x in lines]

probs = softmax(probs, axis=1)
predictions = probs.argmax(axis=1)

conf_mat = confusion_matrix(classes, predictions)

# Saves the list of images in the confusion matrix
#num_classes = conf_mat.shape[0]
#conf_list = [[[] for x in range(num_classes)] for y in range(num_classes)]
#for idx, prediction in enumerate(predictions):
#    conf_list[classes[idx]][prediction].append(idx)
#with open(foldername + '/confmat_list_' + foldername + '.json', 'w') as outfile:
#    json.dump(conf_list, outfile)

# Uncomment this for Matrix reordering
# Matrix reordering
#graph = csr_matrix(conf_mat)
#perm = reverse_cuthill_mckee(graph,symmetric_mode=False)
#conf_mat = conf_mat[perm,:]
#conf_mat = conf_mat[:,perm]

with open(foldername + '/confusion_matrix_' + foldername + '.json', 'w') as outfile:
    json.dump(conf_mat.tolist(), outfile)

with open(foldername + '/confidencescore_' + foldername + '.json', 'w') as outfile:
    json.dump(probs.tolist(), outfile)

# Removing diagonals for better visualization
for i in range(conf_mat.shape[0]):
    conf_mat[i,i]=0

plt.imshow(conf_mat, cmap='Blues', interpolation='nearest')
plt.savefig(foldername + '/cm_' + foldername + '_nodiag.pdf', bbox_inches='tight')


