## Visualizing Confusion Matrices

### Datasets and Models

The datasets and models are available [here](./data_models)

### Web Demo

The implementation is incomplete as the candidate images and images in the cell needs to be updated on cell click.

## Usage
Use python http server to host the webpage. The server will be hosted at localhost:8000

```
$ python -m http.server
```
